from abc import ABC, abstractmethod

import numpy as np
from sklearn.linear_model import LinearRegression

from db import db_manage


class FeatureExtraction(ABC):
    @abstractmethod
    def extract(self, user_id, date_start, date_end, *args):
        """ extract data for user, within date bound constraints
        """
        return


class Slope(FeatureExtraction):

    def clean_dataframe(self, frame):
        # remove NaNs, remove inf
        filt_data = frame[np.isfinite(frame['hr'])]
        filt_data = filt_data[np.isfinite(frame['hrv_hf'])]
        filt_data = filt_data[np.isfinite(frame['temperature_skin'])]
        # drop dep variable
        X = filt_data.drop('temperature_skin', axis=1)
        # drop unneeded vars for lin reg
        X.drop(["date", "_id", "user"], axis=1, inplace=True)
        # this is dep variable
        Y = filt_data.temperature_skin
        return X, Y

    def extract(self, user_id, date_start=None, date_end=None, *args):
        """ send user id and bounding dates, receive slope per
        independent var for it
        """
        data = db_manage.get_data(user_id, date_start=date_start, date_end=date_end)
        X, Y = self.clean_dataframe(data)
        linreg = LinearRegression()
        linreg.fit(X, Y)
        ret = {"user": user_id}
        ret.update(dict(zip(X.keys(), linreg.coef_)))
        return ret
