import datetime

from slope.Slope import Slope


def main():
    slope = Slope()
    slopes = slope.extract(1983, date_start=datetime.datetime(2016, 11, 14, 17, 36, 8))
    print(slopes)

if __name__ == '__main__':
    main()