## AVA challenge code

### Install, run

Clone directory and cd into it.
Create a virtual environment (install virtualenv and virtualenvwrapper):

	mkvirtualenv ava -a . --python==/usr/local/bin/python3
	
Install requirements:

	pip install -r requirements.txt

Install, run MongoDB. Import data:

	mongoimport --db ava --collection health_sample --file ava_health_sample.json
	
Yay! Now you can run the code!

	python ava.py