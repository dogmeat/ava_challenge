import datetime

from pymongo import MongoClient

import pandas as pd


def get_data(user_id, date_start=None, date_end=None):
    """ oooh, a docstring!
    user_id is user id
    from and to date are filters, can be None because
    we process that
    output is filtered mongo data imported into a DataFrame
    """
    client = MongoClient()
    db = client.ava
    col = db.health
    if date_start is None:
        date_start = datetime.datetime.min
    if date_end is None:
        date_end = datetime.datetime.max
    cursor = col.find(
        {
            "user": user_id,
            "date":{
                "$gte": date_start,
                "$lt": date_end,
            }
        }
    )
    return pd.DataFrame(list(cursor))
